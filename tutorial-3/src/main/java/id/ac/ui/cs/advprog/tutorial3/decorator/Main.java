package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
  public static void main(String[] args) {
    Food food = null;
    food = BreadProducer.THIN_BUN.createBreadToBeFilled();
    food = FillingDecorator.CHILI_SAUCE.addFillingToBread(food);
    food = FillingDecorator.LETTUCE.addFillingToBread(food);

    System.out.println("Made \"" + food.getDescription() + "\" with: $" + food.cost());
  }
}