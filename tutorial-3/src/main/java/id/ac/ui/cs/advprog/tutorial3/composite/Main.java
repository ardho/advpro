package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

public class Main {
  public static void main(String[] args) {
    Company company = new Company();

    Ceo ceo = new Ceo("The CEO", 200000);
    company.addEmployee(ceo);

    Cto cto = new Cto("The CTO", 200000);
    company.addEmployee(cto);

    FrontendProgrammer frontendProgrammer = new FrontendProgrammer("The Frontend", 1450000);
    company.addEmployee(frontendProgrammer);

    System.out.println(company.getNetSalaries());

    System.out.println("Employees List:");
    company.getAllEmployees().stream().forEach(Main::printInfo);
  }
  private static void printInfo(Employees employees) {
    System.out.println(employees.getName() + " :" + employees.getRole());
  }
}