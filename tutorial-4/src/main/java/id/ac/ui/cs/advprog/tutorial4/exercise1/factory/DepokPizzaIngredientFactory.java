package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.HotCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.AncolClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheesyBitesDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ABCSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cabbage;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

  public Cheese createCheese() {
    return new HotCheese();
  }

  public Clams createClam() {
    return new AncolClams();
  }

  public Dough createDough() {
    return new CheesyBitesDough();
  }

  public Sauce createSauce() {
    return new ABCSauce();
  }

  public Veggies[] createVeggies() {
    Veggies[] veggies = {new Cabbage(), new Onion()};
    return veggies;
  }

}
