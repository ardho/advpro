package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?

    private static Singleton uniqueInstance = null;

    private Singleton() {}

    public static synchronized Singleton getInstance() {
      if (uniqueInstance == null) {
        uniqueInstance = new Singleton();
      }
      return uniqueInstance;
    }

}
