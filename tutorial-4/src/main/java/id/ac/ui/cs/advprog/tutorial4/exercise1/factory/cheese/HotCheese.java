package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class HotCheese implements Cheese {

  public String toString() {
    return "Hot Cheese";
  }
}
