package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class AncolClams implements Clams{

  public String toString() {
    return "Nice Clams from Ancol";
  }
}
