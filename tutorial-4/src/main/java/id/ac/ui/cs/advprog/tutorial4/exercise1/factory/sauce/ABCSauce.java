package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ABCSauce implements Sauce {
  public String toString() {
    return "ABC sauce";
  }
}
