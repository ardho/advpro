package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AbstractFactoryTest {
  @Test
  public void testNewYorkPizzaStore() {
    PizzaStore nyStore = new NewYorkPizzaStore();
    Pizza pizza = nyStore.orderPizza("veggie");
    assertEquals(pizza.getName(), "New York Style Veggie Pizza");
    pizza = nyStore.orderPizza("clam");
    assertEquals(pizza.getName(), "New York Style Clam Pizza");
    pizza = nyStore.orderPizza("cheese");
    assertEquals(pizza.getName(), "New York Style Cheese Pizza");
  }

  @Test
  public void testDepokPizzaStore() {
    PizzaStore depokStore = new DepokPizzaStore();
    Pizza pizza = depokStore.orderPizza("veggie");
    assertEquals(pizza.getName(), "Depok Style Veggie Pizza");
    pizza = depokStore.orderPizza("clam");
    assertEquals(pizza.getName(), "Depok Style Clam Pizza");
    pizza = depokStore.orderPizza("cheese");
    assertEquals(pizza.getName(), "Depok Style Cheese Pizza");
  }
}