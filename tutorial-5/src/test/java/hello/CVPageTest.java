package hello;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CVController.class)
public class CVPageTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void greetingWithName() throws Exception {
    mockMvc.perform(get("/mycv").param("name", "Ardho")).andExpect(content()
        .string(containsString("Ardho, I hope you interested to hire me")))
        .andExpect(content().string(not("This is my CV")));
  }


  @Test
  public void greetingWithoutName() throws Exception {
    mockMvc.perform(get("/mycv").param("name", ""))
        .andExpect(content().string(containsString("This is my CV")))
        .andExpect(content().string(not(containsString(", I hope you interested to hire me"))));
  }
}
